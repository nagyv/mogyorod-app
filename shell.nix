{ pkgs ? import (fetchTarball "https://channels.nixos.org/nixos-23.05/nixexprs.tar.xz") {} }:

pkgs.mkShell {
  packages = [
    pkgs.git
    pkgs.nodejs_20
  ];
}

# Mogyoród.app

## Funkciók

### Bejelentők

- [ ] Illegális szemét --> Nonprofit
   - location API

### Közösségi érdek

- [ ] Ötletláda
   - szavazás
   - anyagi hozzájárulás (vállalások)

### Üzenetek

- [ ] A bejelentésekről a rendszer a rendszeren belül jelezzen vissza
- [ ] Feliratkozás üzenetekre (alapból senki sincs feliratkozva)
   - Képviselői ülés
   - Helyi rendezvény

### Felhasználókezelés

- [ ] Regisztráció / Bejelentkezés (e-mail/jelszó, Google)
- [ ] Felhasználói adatok törlése

### FB integráció

- [ ] Hello Mogyoródba havi szintű jelentés
   - hány bejelentés volt
   - hogyan állnak az ötletek (1-5 helyezett, új ötletek)

## Tech stack

### Architektúra

A funkciók külön-külön deployolhatók (microservice-like), ennek megfelelően moduláris monorepo.

Nyelv:
- a microservice-k miatt bármi lehet a backend
- javasolt: Javascript, Python

### Backend: 

- Auth: Firebase
- DB: Firebase
- Storage: Firebase
- Compute: 
    - Firebase functions
    - GCP Cloud Run

### Frontend

- NextJS?
- React
- material-ui
- firebase v9+ SDK

### Automatizálás

- Terraform minden infrahoz
- GitLab pipeline

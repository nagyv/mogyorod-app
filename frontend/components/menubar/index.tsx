import MyLink from "@/components/typography/link";

export default function MenuBar() {
  return (<div className="mx-auto max-w-screen-xl px-4 py-8 sm:px-6 sm:py-12 lg:px-8">
  <div className="sm:flex sm:items-center sm:justify-between">
    <div className="text-center sm:text-left">
      <h1 className="text-2xl font-bold text-gray-900 sm:text-3xl">
        Helló Mogyoród!
      </h1>
    </div>

    <div className="mt-4 flex flex-col gap-4 sm:mt-0 sm:flex-row sm:items-center">
      <MyLink href="/esemenyek">Események</MyLink>
      <MyLink href="/otletek">Ötletláda</MyLink>
      <MyLink href="/uzletek">Üzletek, Szakemberek</MyLink>
    </div>
  </div>
</div>)
}
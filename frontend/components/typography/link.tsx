import Link from "next/link"

export default function MyLink({children, href} : {
  children: React.ReactNode, href: string
}) {
  return <Link href={href}  className="hover:bg-sky-700">{children}</Link>
}
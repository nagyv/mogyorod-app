export function H2({children} : {
  children: React.ReactNode
}) {
  return <h2 className="text-xl font-bold text-gray-900 sm:text-3xl py-5">
    {children}
  </h2>
}
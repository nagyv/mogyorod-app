import data from "@/data/uzletek.json"
import { Uzlet, getData } from "../page"
import { H2 } from "@/components/typography/headers"

export async function generateStaticParams() {
  return Object.keys(data).map((slug) => {slug})
}

export default function Page({ params }: { params: { slug: string } }) {
  const { slug } = params
  const uzlet = getData()[slug] as Uzlet
  return <>
    <H2>{uzlet.name}</H2>
    <div>{uzlet.address}</div>
  </>
}
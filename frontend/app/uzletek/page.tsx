import { cache } from 'react'
import { H2 } from "@/components/typography/headers";
import data from "@/data/uzletek.json";
import MyLink from '@/components/typography/link';

export interface Uzlet {
  name: string
  address: string
}

export const getData = cache(() => {
  return data as {[slug:string]: Uzlet}
})

export default async function Page() {
  const data = getData()

  return <>
    <H2>Üzletek, Szakemberek</H2>
    <ul>
      {Object.entries(data).map(([slug, uzlet]) => <li key={`uzlet-${slug}`}>
        <MyLink href={`/uzletek/${slug}`}>{uzlet.name}</MyLink>
        </li>)}
    </ul>
  </>
}
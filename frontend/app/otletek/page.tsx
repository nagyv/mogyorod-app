import { H2 } from "@/components/typography/headers";

export default function Page() {
  return <main className="min-h-screen flex-col items-center justify-between p-24">
    <H2>Ötletláda</H2>
    <ul>
      <li>Első ötlet</li>
      <li>Második ötlet</li>
    </ul>
  </main>
}